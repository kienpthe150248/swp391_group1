<%-- 
    Document   : NavBar
    Created on : Sep 19, 2023, 9:40:53 PM
    Author     : Trung Kien
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<aside class="app-sidebar">
    <div class="app-sidebar__user"><img class="app-sidebar__user-avatar" src="/images/hay.jpg" width="50px"
                                        alt="User Image">
        <div>
            <p class="app-sidebar__user-name"><b>Phan Trung Kiên</b></p>
            <p class="app-sidebar__user-designation">Chào mừng bạn trở lại</p>
        </div>
    </div>
    <hr>
    <ul class="app-menu">
        <li><a id ="home" onclick="homeClick()" class="app-menu__item ${param.home}" href="<c:url value="/view-home" />"><i class='app-menu__icon bx bx-tachometer'></i><span
                    class="app-menu__label">Trang chủ</span></a></li>
        <!--Quản lí store-->
        <li><a id="store" onclick="storeClick()" class="app-menu__item ${param.store}" href="<c:url value="/manager-stores" />"><i class='app-menu__icon bx bx-store'></i> <span
                    class="app-menu__label">Quản lý cửa hàng</span></a></li>

        <li><a id="product" onclick="productClick()" class="app-menu__item ${param.product}" href="<c:url value="#" />"><i class='app-menu__icon bx bx-purchase-tag-alt'>
                </i><span class="app-menu__label">Quản lý sản phẩm</span></a>

        <li><a id="cutomer" onclick="customerClick()" class="app-menu__item ${param.customer}" href="<c:url value="#" />"><i class='app-menu__icon bx  bxs-user-badge'></i>
                <span class="app-menu__label">Quản lý khách hàng</span></a></li>

        <li><a id="seller" onclick="sellerClick()" class="app-menu__item ${param.seller}" href="<c:url value="#" />"><i class='app-menu__icon bx bx-id-card'></i>
                <span class="app-menu__label">Quản lý người bán</span></a></li>

        <li><a id="post" onclick="postClick()" class="app-menu__item ${param.post}" href="<c:url value="#" />"><i class='app-menu__icon bx bx-user-voice'></i>
                <span class="app-menu__label">Quản lý bài đăng</span></a></li>
    </ul>

</aside>

