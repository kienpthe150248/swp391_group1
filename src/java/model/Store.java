/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import dto.StoresDTO;
import java.time.LocalDateTime;

/**
 *
 * @author Trung Kien
 */
public class Store {

    private int stoId;
    private String stoName;
    private String stoAddress;
    private String stoPhone;
    private String stoTimeOpen;
    private String stoTimeClose;
    private Status status;

    public Store() {
    }

    public Store(int stoId, String stoName, String stoAddress, String stoPhone, String stoTimeOpen, String stoTimeClose, Status status) {
        this.stoId = stoId;
        this.stoName = stoName;
        this.stoAddress = stoAddress;
        this.stoPhone = stoPhone;
        this.stoTimeOpen = stoTimeOpen;
        this.stoTimeClose = stoTimeClose;
        this.status = status;
    }

    public static Store toModel(StoresDTO dto) {
        if (dto == null) {
            return null;
        }
        Store model = new Store();
        model.setStoId(dto.getStoId());
        model.setStoName(dto.getStoName());
        model.setStoPhone(dto.getStoPhone());
        model.setStoAddress(dto.getStoAddress());
        model.setStoTimeOpen(dto.getStoTimeOpen());
        model.setStoTimeClose(dto.getStoTimeClose());
        model.setStatus(new Status(dto.getStatusId(), dto.getStatusName()));

        return model;
    }

    public int getStoId() {
        return stoId;
    }

    public void setStoId(int stoId) {
        this.stoId = stoId;
    }

    public String getStoName() {
        return stoName;
    }

    public void setStoName(String stoName) {
        this.stoName = stoName;
    }

    public String getStoAddress() {
        return stoAddress;
    }

    public void setStoAddress(String stoAddress) {
        this.stoAddress = stoAddress;
    }

    public String getStoPhone() {
        return stoPhone;
    }

    public void setStoPhone(String stoPhone) {
        this.stoPhone = stoPhone;
    }

    public String getStoTimeOpen() {
        return stoTimeOpen;
    }

    public void setStoTimeOpen(String stoTimeOpen) {
        this.stoTimeOpen = stoTimeOpen;
    }

    public String getStoTimeClose() {
        return stoTimeClose;
    }

    public void setStoTimeClose(String stoTimeClose) {
        this.stoTimeClose = stoTimeClose;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

}
