/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.Date;

/**
 *
 * @author Admin
 */
public class Blog {
    private int blogId;
    private String blogTitle;
    private String blogDetail;
    private Date blogDate;
    private int categoryBlogId;
    private int blogImgId;
    private int status;

    public Blog() {
    }

    public Blog(int blogId, String blogTitle, String blogDetail, Date blogDate, int categoryBlogId, int blogImgId, int status) {
        this.blogId = blogId;
        this.blogTitle = blogTitle;
        this.blogDetail = blogDetail;
        this.blogDate = blogDate;
        this.categoryBlogId = categoryBlogId;
        this.blogImgId = blogImgId;
        this.status = status;
    }

    public int getBlogId() {
        return blogId;
    }

    public void setBlogId(int blogId) {
        this.blogId = blogId;
    }

    public String getBlogTitle() {
        return blogTitle;
    }

    public void setBlogTitle(String blogTitle) {
        this.blogTitle = blogTitle;
    }

    public String getBlogDetail() {
        return blogDetail;
    }

    public void setBlogDetail(String blogDetail) {
        this.blogDetail = blogDetail;
    }

    public Date getBlogDate() {
        return blogDate;
    }

    public void setBlogDate(Date blogDate) {
        this.blogDate = blogDate;
    }

    public int getCategoryBlogId() {
        return categoryBlogId;
    }

    public void setCategoryBlogId(int categoryBlogId) {
        this.categoryBlogId = categoryBlogId;
    }

    public int getBlogImgId() {
        return blogImgId;
    }

    public void setBlogImgId(int blogImgId) {
        this.blogImgId = blogImgId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
    
    
}
