/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author Admin
 */
public class CategoryBlog {
    private int caBlogId;
    private String caBlogName;
    private int status;

    public CategoryBlog() {
    }

    public CategoryBlog(int caBlogId, String caBlogName, int status) {
        this.caBlogId = caBlogId;
        this.caBlogName = caBlogName;
        this.status = status;
    }

    public int getCaBlogId() {
        return caBlogId;
    }

    public void setCaBlogId(int caBlogId) {
        this.caBlogId = caBlogId;
    }

    public String getCaBlogName() {
        return caBlogName;
    }

    public void setCaBlogName(String caBlogName) {
        this.caBlogName = caBlogName;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
    
    
}
