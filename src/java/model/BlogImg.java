/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author Admin
 */
public class BlogImg {
    private int blogImgId;
    private String blogImgUrl;
    private int blogId;

    public BlogImg() {
    }

    public BlogImg(int blogImgId, String blogImgUrl, int blogId) {
        this.blogImgId = blogImgId;
        this.blogImgUrl = blogImgUrl;
        this.blogId = blogId;
    }

    public int getBlogImgId() {
        return blogImgId;
    }

    public void setBlogImgId(int blogImgId) {
        this.blogImgId = blogImgId;
    }

    public String getBlogImgUrl() {
        return blogImgUrl;
    }

    public void setBlogImgUrl(String blogImgUrl) {
        this.blogImgUrl = blogImgUrl;
    }

    public int getBlogId() {
        return blogId;
    }

    public void setBlogId(int blogId) {
        this.blogId = blogId;
    }
    
    
    
}
