/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author Admin
 */
public class ProductSize {
    private int pSizeId;
    private int productId;
    private Size size;
    private int quantity;
    private double price;

    public ProductSize() {
    }

    public ProductSize(int pSizeId, int productId, Size size, int quantity, double price) {
        this.pSizeId = pSizeId;
        this.productId = productId;
        this.size = size;
        this.quantity = quantity;
        this.price = price;
    }

    public int getpSizeId() {
        return pSizeId;
    }

    public void setpSizeId(int pSizeId) {
        this.pSizeId = pSizeId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public Size getSize() {
        return size;
    }

    public void setSize(Size size) {
        this.size = size;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    
}