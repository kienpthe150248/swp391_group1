/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

package model;

import java.util.List;

public class Product {
    private int pid;
    private String pName;
    private String pDetail;
    private String pBrand;
    private String pSKU;
    
    private Category category;
    private Status status;
    private Store store;
    private List<Size> size;
    private List<Image> image;

    public Product() {
    }

    public Product(int pid, String pName, String pDetail, String pBrand, String pSKU, Category category, Status status, Store store, List<Size> size, List<Image> image) {
        this.pid = pid;
        this.pName = pName;
        this.pDetail = pDetail;
        this.pBrand = pBrand;
        this.pSKU = pSKU;
        this.category = category;
        this.status = status;
        this.store = store;
        this.size = size;
        this.image = image;
    }

    public int getPid() {
        return pid;
    }

    public void setPid(int pid) {
        this.pid = pid;
    }

    public String getpName() {
        return pName;
    }

    public void setpName(String pName) {
        this.pName = pName;
    }

    public String getpDetail() {
        return pDetail;
    }

    public void setpDetail(String pDetail) {
        this.pDetail = pDetail;
    }

    public String getpBrand() {
        return pBrand;
    }

    public void setpBrand(String pBrand) {
        this.pBrand = pBrand;
    }

    public String getpSKU() {
        return pSKU;
    }

    public void setpSKU(String pSKU) {
        this.pSKU = pSKU;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }

    public List<Size> getSize() {
        return size;
    }

    public void setSize(List<Size> size) {
        this.size = size;
    }

    public List<Image> getImage() {
        return image;
    }

    public void setImage(List<Image> image) {
        this.image = image;
    }
    
    

}
