/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package paging;

/**
 *
 * @author Trung Kien
 */
public interface Pageble {
    Integer getPage();
    Integer getOffset();
    Integer getLimit();
}
