/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dto;

import model.Status;
import model.Store;
import java.time.LocalDateTime;

/**
 *
 * @author Trung Kien
 */
public class StoresDTO {

    private int stoId;
    private String stoName;
    private String stoAddress;
    private String stoPhone;
    private String stoTimeOpen;
    private String stoTimeClose;
    private Integer statusId;
    private String statusName;

    public StoresDTO() {
    }

    public StoresDTO(String stoName, String stoAddress, String stoPhone, String stoTimeOpen, String stoTimeClose, Integer statusId) {
        this.stoName = stoName;
        this.stoAddress = stoAddress;
        this.stoPhone = stoPhone;
        this.stoTimeOpen = stoTimeOpen;
        this.stoTimeClose = stoTimeClose;
        this.statusId = statusId;
    }
    

    public static StoresDTO toDto(Store store) {
        if (store == null) {
            return null;
        }
        StoresDTO dto = new StoresDTO();
        dto.setStoId(store.getStoId());
        dto.setStoName(store.getStoName());
        dto.setStoPhone(store.getStoPhone());
        dto.setStoAddress(store.getStoAddress());
        dto.setStoTimeOpen(store.getStoTimeOpen());
        dto.setStoTimeClose(store.getStoTimeClose());
        dto.setStatusId(store.getStatus().getStid());
        dto.setStatusName(store.getStatus().getStName());

        return dto;
    }

    public int getStoId() {
        return stoId;
    }

    public void setStoId(int stoId) {
        this.stoId = stoId;
    }

    public String getStoName() {
        return stoName;
    }

    public void setStoName(String stoName) {
        this.stoName = stoName;
    }

    public String getStoAddress() {
        return stoAddress;
    }

    public void setStoAddress(String stoAddress) {
        this.stoAddress = stoAddress;
    }

    public String getStoPhone() {
        return stoPhone;
    }

    public void setStoPhone(String stoPhone) {
        this.stoPhone = stoPhone;
    }

    public String getStoTimeOpen() {
        return stoTimeOpen;
    }

    public void setStoTimeOpen(String stoTimeOpen) {
        this.stoTimeOpen = stoTimeOpen;
    }

    public String getStoTimeClose() {
        return stoTimeClose;
    }

    public void setStoTimeClose(String stoTimeClose) {
        this.stoTimeClose = stoTimeClose;
    }

    public Integer getStatusId() {
        return statusId;
    }

    public void setStatusId(Integer statusId) {
        this.statusId = statusId;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }
    

}
