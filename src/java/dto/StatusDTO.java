/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dto;

import model.Status;

/**
 *
 * @author Trung Kien
 */
public class StatusDTO {

    private int statusId;
    private String statusName;

    public StatusDTO() {
    }

    public StatusDTO(int statusId, String statusName) {
        this.statusId = statusId;
        this.statusName = statusName;
    }

    public static StatusDTO toDto(Status status) {
        StatusDTO sdto = new StatusDTO();
        sdto.setStatusId(status.getStid());
        sdto.setStatusName(status.getStName());
        return sdto;
    }

    public int getStatusId() {
        return statusId;
    }

    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

}
