/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package service;

import model.Store;
import DAL.StoreDAO;
import dto.StoresDTO;
import jakarta.inject.Named;
import java.util.ArrayList;
import java.util.List;
import service.interfaces.IStoreService;

/**
 *
 * @author Trung Kien
 */
@Named
public class StoreService implements IStoreService {

    private final StoreDAO storeDAO = new StoreDAO();

    @Override
    public boolean insertStore(StoresDTO storeDTO) {
        Store store = Store.toModel(storeDTO);

        return storeDAO.create(store);
    }

    @Override
    public List<StoresDTO> getListStores() {
        List<StoresDTO> list = new ArrayList<>();
        List<Store> storeses = storeDAO.findAll();
        for (Store stores : storeses) {
            list.add(StoresDTO.toDto(stores));
        }
        return list;
    }

}
