/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package service.interfaces;

import dto.StatusDTO;
import java.util.List;

/**
 *
 * @author Trung Kien
 */
public interface IStatusService {
    List<StatusDTO> findAll();
}
