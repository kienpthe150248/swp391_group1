/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controller.admin;

import dto.StatusDTO;
import dto.StoresDTO;
import jakarta.inject.Inject;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import service.interfaces.IStatusService;
import service.interfaces.IStoreService;
import util.StringUtils;

/**
 *
 * @author Trung Kien
 *
 */
@WebServlet(name = "ManagerStoreController", value = "/manager-stores")
public class StoreController extends HttpServlet {

    @Inject
    private IStoreService iStoreService;

    @Inject
    private IStatusService iStatusService;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        resp.setCharacterEncoding("UTF-8");
        String action = req.getParameter("action");
        String stoName = req.getParameter("stoName");
        String stoAddress = req.getParameter("stoAddress");
        String stoPhone = req.getParameter("stoPhone");
        String timeOpen = req.getParameter("stoTimeOpen");
        String timeClose = req.getParameter("stoTimeClose");
        String statusStr = req.getParameter("statusId");
        int statusId = StringUtils.isBlank(statusStr) ? null : Integer.parseInt(statusStr);
        if (action.equals("add-store")) {
            StoresDTO storeDTO = new StoresDTO(stoName, stoAddress, stoPhone, timeOpen, timeClose, statusId);
            iStoreService.insertStore(storeDTO);
            resp.sendRedirect(req.getContextPath() + "/manager-stores");
        }

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        resp.setCharacterEncoding("UTF-8");
        String action = req.getParameter("action");
        if (action == null) {
            List<StoresDTO> storesDTOs = iStoreService.getListStores();

            req.setAttribute("listStores", storesDTOs);

            RequestDispatcher rd = req.getRequestDispatcher("stores.jsp");
            rd.forward(req, resp);
        } else if (action.equals("add-store")) {
            List<StatusDTO> statusDTOs = iStatusService.findAll();
            req.setAttribute("statuses", statusDTOs);
            RequestDispatcher rd = req.getRequestDispatcher("addStore.jsp");
            rd.forward(req, resp);
        }
    }

}
