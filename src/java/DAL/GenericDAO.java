/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAL;

import paging.Pageble;
import java.util.List;

/**
 *
 * @author Trung Kien
 * @param <T>
 * @param <ID>
 */
public interface GenericDAO<T,ID> {
    boolean create(T data);
    boolean update(T data);
    boolean deleteById(ID id);
    List<T> findAll();
    List<T> findAll(Pageble pageble);
}
