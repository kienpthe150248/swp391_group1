/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAL;

import model.Status;
import model.Store;
import paging.Pageble;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Trung Kien
 */
public class StoreDAO implements GenericDAO<Store, Integer> {

    public StoreDAO() {

    }

    @Override
    public boolean create(Store data) {
        String sql = "insert into Stores(stoName,stoAddress,stoPhone,stoTimeOpen,stoTimeClosed,stid) values(?,?,?,?,?,?)";
        try (Connection con = DBContext.connect()) {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, data.getStoName());
            ps.setString(2, data.getStoAddress());
            ps.setString(3, data.getStoPhone());
            ps.setString(4, data.getStoTimeOpen());
            ps.setString(5, data.getStoTimeClose());
            ps.setInt(6, data.getStatus().getStid());

            return ps.executeUpdate() != 0;
        } catch (SQLException ex) {
            Logger.getLogger(StoreDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    @Override
    public boolean update(Store data) {
        String sql = "update Store set stoName=?, stoAddress=?, stoPhone=?, stoTimeOpen=?, stoTimeClose=?, stid=? where stoId=?";

        try (Connection con = DBContext.connect()) {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, data.getStoName());
            ps.setString(2, data.getStoAddress());
            ps.setString(3, data.getStoPhone());
            ps.setString(4, data.getStoTimeOpen());
            ps.setString(5, data.getStoTimeClose());
            ps.setInt(6, data.getStatus().getStid());
            ps.setInt(7, data.getStoId());

            return ps.executeUpdate() != 0;
        } catch (SQLException ex) {
            Logger.getLogger(StoreDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    @Override
    public boolean deleteById(Integer id) {

        String sql = "delete from Store where storeId=?";
        try (Connection con = DBContext.connect()) {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1, id);
            return ps.executeUpdate() != 0;
        } catch (SQLException ex) {
            Logger.getLogger(StoreDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    @Override
    public List<Store> findAll() {
        String sql = "select * from Stores s left join Status st on s.stid = st.stid";
        List<Store> list = new ArrayList<>();
        try (Connection con = DBContext.connect()) {

            PreparedStatement ps = con.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Store store = new Store(
                        rs.getInt("stoId"),
                        rs.getString("stoName"),
                        rs.getString("stoAddress"),
                        rs.getString("stoPhone"),
                        rs.getString("stoTimeOpen"),
                        rs.getString("stoTimeClosed"),
                        new Status(rs.getInt("stid"), rs.getString("stName"))
                );
                list.add(store);
            }
        } catch (SQLException ex) {
            Logger.getLogger(StoreDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    @Override
    public List<Store> findAll(Pageble pageble) {
        return new ArrayList<>();
    }

}
