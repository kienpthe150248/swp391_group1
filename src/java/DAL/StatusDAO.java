/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAL;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Status;
import paging.Pageble;

/**
 *
 * @author Trung Kien
 */
public class StatusDAO implements GenericDAO<Status, Integer> {

    @Override
    public boolean create(Status data) {
        return false;
    }

    @Override
    public boolean update(Status data) {
        return false;
    }

    @Override
    public boolean deleteById(Integer id) {
        return false;
    }

    @Override
    public List<Status> findAll() {
        List<Status> status = new ArrayList<>();
        String sql = "select * from Status";
        try (Connection con = DBContext.connect()) {
            PreparedStatement ps = con.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Status st = new Status(rs.getInt("stid"), rs.getString("stName"));
                status.add(st);
            }
        } catch (SQLException ex) {
            Logger.getLogger(StatusDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return status;
    }

    @Override
    public List<Status> findAll(Pageble pageble) {
        List<Status> status = new ArrayList<>();

        return status;
    }

}
