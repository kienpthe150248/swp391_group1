/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package util;

/**
 *
 * @author Trung Kien
 */
public class StringUtils {
    public static boolean isBlank(String text) {
        return text == null || text.equals("");
    }
}
