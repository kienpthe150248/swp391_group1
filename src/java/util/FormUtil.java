///*
// * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
// * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
// */
//package util;
//
//import jakarta.servlet.http.HttpServletRequest;
//import java.lang.reflect.InvocationTargetException;
//import org.apache.commons.beanutils.BeanUtils;
//
///**
// *
// * @author Trung Kien
// */
//public class FormUtil {
//
//    @SuppressWarnings("unchecked")
//    public static <T> T toModel(Class<T> clazz, HttpServletRequest request) {
//        T object = null;
//        try {
//            object = clazz.newInstance();
//            BeanUtils.populate(object, request.getParameterMap());
//        } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
//            System.out.print(e.getMessage());
//        }
//        return object;
//    }
//}
